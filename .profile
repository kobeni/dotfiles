#!/bin/sh

#	$HOME/.profile
# All users who have the shell, sh(1), as their login command have the commands
# in these files  executed as part of their login sequence.

# Default applications
export TERMINAL=alacritty
export BROWSER=badwolf
export EDITOR=vi

# Handy
export GITDIR=$HOME/.local/git

# UNIX Variables
export PATH=$HOME/.local/bin:/usr/bin
export ENV=$XDG_CONFIG_HOME/alias
export PS1='\w \$ '

# PASH/PFETCH Variables
export PASH_DIR=$GITDIR/kobeni/pass
export PASH_PATTERN='A-z0-9-_:;[]().,!@#$%^&*?'
export PASH_KEYID=$HOME/.local/share/ssh/kobeni
export PF_INFO="ascii title os kernel uptime pkgs memory palette"

# Start X server if log on tty1, else run another shell
[ `tty` = /dev/tty1 ] && 
	XAUTHORITY=$XDG_CONFIG_HOME/X11/.Xauthority  \
	exec sx $XDG_CONFIG_HOME/X11/xinitrc 2>/dev/null
